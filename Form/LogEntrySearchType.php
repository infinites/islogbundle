<?php

namespace InfiniteSoftware\Bundle\ISLogBundle\Form;

use InfiniteSoftware\Bundle\ISLogBundle\Services\LogManager;
use InfiniteSoftware\Bundle\ISLogBundle\Stuff\LogEntrySearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class LogEntrySearchType extends AbstractType
{
    /**
     * @var LogManager
     */
    private $logManager;

    /**
     * LogEntrySearchType constructor.
     * @param LogManager $logManager
     */
    function __construct(LogManager $logManager)
    {
        $this->logManager = $logManager;
    }

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * @var LogEntrySearch $es
         */
        $es = $options['data'];

        $builder
            // Here style is used to make field hidden and save field type as "integer".
            ->add('offset', IntegerType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'style' => 'display: none;'
                ]
            ])

            ->add('limit', IntegerType::class, [
                'required' => false,
                'label' => false,
                'empty_data' => (string) $es->getLimit(),
                'attr' => [
                    'style' => 'display: none;'
                ]
            ])

            ->add('logType', IntegerType::class, [
                'label' => false,
                'required' => false,
                'empty_data' => (string) $es->getLogType(),
                'attr' => [
                    'style' => 'display: none;'
                ]
            ])

            ->add('sortField', TextType::class, [
                'required' => false,
                'label' => false,
                'empty_data' => $es->getSortField(),
                'attr' => [
                    'style' => 'display: none;'
                ]
            ])

            ->add('sortMethod', TextType::class, [
                'required' => false,
                'label' => false,
                'empty_data' => $es->getSortMethod(),
                'attr' => [
                    'style' => 'display: none;'
                ]
            ])

            ->add('searchField', TextType::class, [
                'required' => false,
                'label' => false,
                'empty_data' => $es->getSearchField(),
                'attr' => [
                    'style' => 'display: none;'
                ]
            ])

            ->add('searchString', TextType::class, [
                'required' => false,
                'label' => false,
                'empty_data' => $es->getSearchString(),
                'attr' => [
                    'style' => 'display: none;'
                ]
            ])
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => LogEntrySearch::class,
                'csrf_protection' => false // TODO: Is it ok? Necessary for API...
            ])
        ;
    }
}