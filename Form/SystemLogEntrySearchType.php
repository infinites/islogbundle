<?php

namespace InfiniteSoftware\Bundle\ISLogBundle\Form;

use InfiniteSoftware\Bundle\ISLogBundle\Services\LogManager;
use InfiniteSoftware\Bundle\ISLogBundle\Stuff\LogEntrySearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class SystemLogEntrySearchType extends AbstractType
{
    /**
     * @var LogManager
     */
    private $logManager;

    /**
     * LogEntrySearchType constructor.
     * @param LogManager $logManager
     */
    function __construct(LogManager $logManager)
    {
        $this->logManager = $logManager;
    }

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // Here style is used to make field hidden and save field type as "integer".
            ->add('offset', IntegerType::class, [
                'label' => false,
                'attr' => [
                    'style' => 'display: none;'
                ]
            ])
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => LogEntrySearch::class
            ])
        ;
    }
}