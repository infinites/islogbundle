<?php
declare(strict_types=1);

namespace InfiniteSoftware\Bundle\ISLogBundle\Services;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Service for general purpose functionality.
 *
 * Class HelpManager
 * @package InfiniteSoftware\Bundle\ISLogBundle\Services
 */
class HelpManager
{
    /**
     * @var ValidatorInterface
     */
    private $validator;


    public function __construct(
        ValidatorInterface $validator
    )
    {
        $this->validator = $validator;
    }

    /**
     * @param object $object
     * @return null|string
     */
    public function getFirstViolationMsg($object): ?string
    {
        $violations = $this->validator->validate($object);

        if ($violations->count())
        {
            /**
             * @var ConstraintViolation $violation
             */
            foreach ($violations as $violation)
            {
                return get_class($violation->getRoot()) . '::' . $violation->getPropertyPath() . ': ' . $violation->getMessage();
            }
        }

        return null;
    }

    /**
     * Source: https://knpuniversity.com/screencast/symfony-rest2/validation-errors-response
     * @param FormInterface $form
     * @return array
     */
    public function getFormErrors(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getFormErrors($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
}