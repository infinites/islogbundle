<?php


namespace InfiniteSoftware\Bundle\ISLogBundle\Services;

use InfiniteSoftware\Bundle\ISLogBundle\Document\AdminLogEntry;
use InfiniteSoftware\Bundle\ISLogBundle\Document\AppLogEntry;
use InfiniteSoftware\Bundle\ISLogBundle\Document\SystemLogEntry;
use InfiniteSoftware\Bundle\ISLogBundle\Document\TraderLogEntry;
use InfiniteSoftware\Bundle\ISLogBundle\Document\TransactionLogEntry;
use InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository\AdminLogEntryRepository;
use InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository\AppLogEntryRepository;
use InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository\SystemLogEntryRepository;
use InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository\TraderLogEntryRepository;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository\TransactionLogEntryRepository;
use InfiniteSoftware\Bundle\ISLogBundle\Stuff\LogEntrySearch;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Validation;

/**
 * Class LogManager
 * @package InfiniteSoftware\Bundle\ISLogBundle\Services
 */
class LogManager
{
    const STATUS_INFO = 'info';
    const STATUS_WARNING = 'warning';
    const STATUS_DANGER = 'danger';

    const LOG_TYPE_SYS = 0;
    const LOG_TYPE_TRANSACTION = 1;
    const LOG_TYPE_ADMIN = 2;
    const LOG_TYPE_TRADER = 3;
    const LOG_TYPE_APP = 4;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var ManagerRegistry
     */
    private $dm;

    /**
     * @var Validation
     */
    private $validator;

    /**
     * @var int list limit for extra (additional)
     *      news fetching (AJAX, pagination etc.)
     */
    private $listLimit = 20;

    private $email;

    private $componentName;

    /**
     * TODO: Templating injection not working.
     * @var TwigEngine
     */
    #private $templating;

    /**
     * LogManager constructor.
     * @param ManagerRegistry $dm
     * @param \Swift_Mailer $mailer
     * @param string $email
     * @param string $componentName
     */
    public function __construct(
        ManagerRegistry $dm,
        \Swift_Mailer $mailer,
        $email,
        $componentName
    )
    {
        $this->dm = $dm->getManager();
        $this->mailer = $mailer;
        $this->validator = Validation::createValidator();
        $this->email = $email;
        $this->componentName = $componentName;
    }

//    TODO: Maybe remove.
//    private function getEntryById($entryId)
//    {
//        //return $this->getLogRepository()->find($entryId);
//    }

    /**
     * @param $entry
     * @return bool
     */
    private function insertEntry($entry)
    {
        $this->dm->persist($entry);
        $this->flush();
        return true;
    }

    /**
     * @param int $logType
     * @return bool
     */
    public function emptyLog($logType)
    {
        $repo = $this->getLogRepository($logType);

        $repo->emptyLog();

        return true;
    }

//    TODO: Maybe remove.
//    private function deleteEntryById($entryId)
//    {
//        $this->dm->remove($this->getEntryById($entryId));
//        $this->dm->flush();
//
//        $totalEntries = $this->getLogRepository()->countLogEntries();
//
//        return [
//            'total' => $totalEntries,
//            'lastPageNum' => $this->getLastPageNumber($totalEntries)
//        ];
//    }

    /**
     * @param string $logType
     * @param string $listLimit
     * @param array $ids
     * @return array
     */
    public function deleteEntryByIds($logType, $listLimit, array $ids)
    {
        $repo = $this->getLogRepository($logType);

        $repo->deleteEntryByIds($ids);

        $totalEntries = $repo->countLogEntries();

        return [
            'total' => $totalEntries,
            'lastPageNum' => $this->getLastPageNumber($totalEntries, (int) $listLimit)
        ];
    }

    private function flush()
    {
        $this->dm->flush();
    }

    /**
     * @param SystemLogEntry|AppLogEntry $entry
     * @return bool
     */
    private function log($entry)
    {
        $this->validateEntry($entry);

        $this->insertEntry($entry);

        return true;
    }

    /**
     * @param \Exception $ex
     * @param string $tag
     * @return \Exception
     * @throws \Exception
     */
    public function ex(\Exception $ex, $tag = '#exception')
    {
        $class = (new \ReflectionClass($ex))->getShortName();
        $code = $ex->getCode();
        $msg = $ex->getMessage();
        $file = $ex->getFile();
        $line = $ex->getLine();

        $this->logSysEntry(
            LogManager::STATUS_DANGER,
            $msg, $class, $code,
            $file, $line, $tag
        );

        return $ex;
    }

    /**
     * TODO: Maybe remove.
     * @param AppLogEntry|SystemLogEntry $entry
     * @throws \Exception
     * @return boolean
     */
    private function validateEntry($entry)
    {
        if (
            $entry->getClassShortName() !== 'AppLogEntry'
            && $entry->getClassShortName() !== 'SystemLogEntry'
        )
        {
            throw new \Exception('Invalid log entry. Entry must be object of any of this classes: AppLogEntry or SystemLogEntry');
        }

        return true;
    }


    /**
     * @param LogEntry $logEntry
     * @return boolean - true if everything is ok.
     */
    private function email(LogEntry $logEntry)
    {
        /** @var \Swift_Message $message */
        $message = $this->mailer->createMessage();
        $message
            ->setTo($this->logEmail)
            ->setSubject($logEntry->getStatus() . ' #' . time())
            ->setFrom('no-reply@cryptops.ru')
            ->setBody(
                $this->templating->render(
                    '@DevAdmin/Log/entry.email.html.twig', [
                        'entry' => $logEntry,
                    ]
                ), 'text/html'
            )
        ;

        $this->mailer->send($message);

        return true;
    }

    /**
     * @param LogEntrySearch $logEntrySearch
     * @return array
     * @throws \Exception
     */
    public function getAppLog(LogEntrySearch $logEntrySearch)
    {
        $log =  $this->getAppLogRepository()->getLog($logEntrySearch);
        $totalEntries = $log->count();
        $entries = [];

        foreach ($log as $entry) {
            $entries[] = $entry;
        }

        return [
            'entries' => $entries,
            'total' => $log->count(),
            'limit' => $logEntrySearch->getLimit(),
            'lastPageNum' => $this->getLastPageNumber($totalEntries, $logEntrySearch->getLimit())
        ];
    }

    /**
     * @param LogEntrySearch $logEntrySearch
     * @return array
     * @throws \Exception
     */
    public function getSystemLog(LogEntrySearch $logEntrySearch)
    {
        $log =  $this->getSystemLogRepository()->getLog($logEntrySearch);
        $totalEntries = $log->count();
        $entries = [];

        foreach ($log as $entry) {
            $entries[] = $entry;
        }

        return [
            'entries' => $entries,
            'total' => $log->count(),
            'limit' => $logEntrySearch->getLimit(),
            'lastPageNum' => $this->getLastPageNumber($totalEntries, $logEntrySearch->getLimit())
        ];
    }

    /**
     * Necessary in JS pagination plugin.
     *
     * @param int $total - total log entries.
     * @param int $limit - total log entries.
     * @return int
     */
    private function getLastPageNumber($total, $limit)
    {
        $num = ceil($total / $limit);

        if ($num == 0) return 1;
        else return (int) $num;
    }

    /**
     * @return bool
     */
    public function generateFixtures()
    {
        // Firstly delete all previous entries.
        $this->getAppLogRepository()->emptyLog();

        $status = [self::STATUS_INFO, self::STATUS_WARNING, self::STATUS_DANGER];

        for ($i = 0; $i < 24; $i++)
        {
            // LOG_TYPE_TRANSACTION
            $this->logTxEntry(
                $status[rand(0, count($status) - 1)], $i + 1,
                'CryptoFiat', $i + rand(1, 999), 'received',
                false, 'this is tx'
            );
        }

        for ($i = 0; $i < 24; $i++)
        {
            // LOG_TYPE_ADMIN
            $this->logAdminEntry(
                $status[rand(0, count($status) - 1)], $i + 1,
                'ban', 'user is banned', rand(1, 999)
            );
        }

        for ($i = 0; $i < 24; $i++)
        {
            // LOG_TYPE_ADMIN
            $this->logTraderEntry(
                $status[rand(0, count($status) - 1)], $i + 1,
                'log in', 'trader is logged in'
            );
        }

        for ($i = 0; $i < 24; $i++)
        {
            $this->ex(new \Exception('my exception' . uniqid()));
        }

        return true;
    }

    /**
     * @return int
     */
    public function getListLimit()
    {
        return $this->listLimit;
    }

    /**
     * @param string $status
     * @param string $message
     * @param null|string $exception
     * @param null|string $code
     * @param null|string $file
     * @param null|string $line
     * @param null|string $tag
     * @return bool
     */
    public function logSysEntry(
        $status,
        $message,
        $exception = null,
        $code = null,
        $file = null,
        $line = null,
        $tag = null
    ) {
        $log = new SystemLogEntry($status, $message, $this->componentName);

        if ($exception) $log->setException($exception);
        if ($code) $log->setCode($code);
        if ($file) $log->setFile($file);
        if ($line) $log->setLine($line);
        if ($tag) $log->setTag($tag);

        $this->log($log);

        return true;
    }

    /**
     * @param string $status
     * @param int $adminId
     * @param string $action
     * @param string $message
     * @param null|int $traderId
     * @param null|string $tag
     * @return bool
     */
    public function logAdminEntry($status, $adminId, $action, $message, $traderId = null, $tag = null)
    {
        $log = new AppLogEntry(self::LOG_TYPE_ADMIN, $status, $message);
        $log->setAdminId($adminId);
        $log->setAction($action);

        if ($traderId) $log->setTraderId($traderId);
        if ($tag) $log->setTag($tag);

        $this->log($log);

        return true;
    }

    /**
     * @param string $status
     * @param int $traderId
     * @param string $action
     * @param string $message
     * @param null|string $tag
     * @return bool
     */
    public function logTraderEntry($status, $traderId, $action, $message, $tag = null)
    {
        $log = new AppLogEntry(self::LOG_TYPE_TRADER, $status, $message);
        $log->setTraderId($traderId);
        $log->setAction($action);

        if ($tag) $log->setTag($tag);

        $this->log($log);

        return true;
    }

    /**
     * @param string $status
     * @param $traderId
     * @param string $txType
     * @param string $txCode
     * @param string $txStatus
     * @param bool $txSub
     * @param string $message
     * @param null|string $tag
     * @return bool
     */
    public function logTxEntry($status, $traderId, $txType, $txCode, $txStatus, $txSub, $message, $tag = null)
    {
        $log = new AppLogEntry(self::LOG_TYPE_TRANSACTION, $status, $message);
        $log->setTraderId($traderId);
        $log->setTxType($txType);
        $log->setTxCode($txCode);
        $log->setTxStatus($txStatus);
        $log->setTxSub($txSub);

        if ($tag) $log->setTag($tag);

        $this->log($log);

        return true;
    }

    /**
     * @param int $logType
     * @return SystemLogEntryRepository| AppLogEntryRepository
     * @throws \Exception
     */
    private function getLogRepository($logType)
    {
        if ($logType === self::LOG_TYPE_SYS)
        {
            return $this->getSystemLogRepository();
        }
        else
        {
            return $this->getAppLogRepository();
        }
    }

    /**
     * @return SystemLogEntryRepository
     */
    private function getSystemLogRepository() {
        return $this->dm->getRepository('ISLogBundle:SystemLogEntry');
    }

    /**
     * @return AppLogEntryRepository
     */
    private function getAppLogRepository() {
        return $this->dm->getRepository('ISLogBundle:AppLogEntry');
    }
}