# ISLogBundle Documentation

### Requirements
1. You should have configured MongoDB where logs will be written, using [doctrine/mongodb-odm-bundle](https://packagist.org/packages/doctrine/mongodb-odm-bundle).
2. [twig/extensions](https://packagist.org/packages/twig/extensions) should be installed. Do not 
   forget in app/services.yml define a service after bundle installation:
    
        twig.extension.intl:
          class: Twig_Extensions_Extension_Intl
          tags:
              - { name: twig.extension }

### Installation
1. composer require infinitesoftware/is-log-bundle
2. Modify AppKernel.php as always:

        // app/AppKernel.php
        
        // ...
        class AppKernel extends Kernel
        {
            // ...
        
            public function registerBundles()
            {
                $bundles = array(
                    // ...
                    new InfiniteSoftware\Bundle\ISLogBundle\ISLogBundle(),
                );
        
                // ...
            }
        }
        
4. Add parameters to app/config.yml:

        is_log:
          email: 'lol@example.com' # (Not mandatory) email where log entries will be sent
          component_name: 'main' # (Mandatory) system's name where log is used
        
3. In app/routing_dev.yml include:

        is_log:
            resource: "@ISLogBundle/Resources/config/routing.yml"
            prefix:   /log
        
    Now you can visit the log by url: .../app_dev.php/log
    If you see page without css run in terminal: php bin/console assets:install --symlink
        
    
### Usage 

Exists three types of log:
1. SystemLog
2. TraderLog
3. AdminLog
4. TransactionLog

To write to any of this log you need service from the bundle - ISLogManager.
For example in your Action you can do:

    public function indexAction(LogManager $logManager)
    {
            // $logManager = $this->get('is_log.manager.log_manager');
            
            $logManager->logSysEntry(LogManager::STATUS_WARNING, 'my warning msg');
    
            $logManager->logAdminEntry(LogManager::STATUS_INFO, 4, 'ban', 'user is banned', null, '#test');
    
            $logManager->logTraderEntry(LogManager::STATUS_INFO, 13, 'log in', 'trader is logged in');
    
            $logManager->logTxEntry(LogManager::STATUS_INFO, 13, 'Exchange', 144, 'received', false, 'money is received');
    
    
            // Exception automatically will be written to System Log.
            // Return passed exception, so it can be thrown, e.g.
            // throw $logManager->ex(new LogicException('My exception'));
            $logManager->ex(new LogicException('My exception'));
    
            // ...
     }
