<?php

namespace InfiniteSoftware\Bundle\ISLogBundle\Controller;

use InfiniteSoftware\Bundle\ISLogBundle\Services\LogManager;
use InfiniteSoftware\Bundle\ISLogBundle\Stuff\LogEntrySearch;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Asset\Exception\LogicException;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $logManager = $this->get('is_log.manager.log_manager');

//        $logManager->generateFixtures();

//        $logSearch = new LogEntrySearch(LogManager::LOG_TYPE_TRADER);
//        $logSearch->setSearchField('message');
//        $logSearch->setSearchString('zbs');
//
//        $res = $logManager->getAppLogRepository()->getLog($logSearch);
//
//        foreach ($res as $entry)
//        {
//            dump($entry);
//        }


//        $logManager->logSysEntry(LogManager::STATUS_WARNING, 'my warning msg');
//
//        $logManager->logAdminEntry(LogManager::STATUS_INFO, 4, 'ban', 'user is banned', null, '#test');
//
//        $logManager->logTraderEntry(LogManager::STATUS_INFO, 13, 'log in', 'trader is logged in');
//
//        $logManager->logTxEntry(LogManager::STATUS_INFO, 13, 'Exchange', 144, 'received', false, 'money is received');
//
//        throw $logManager->ex(new \Exception('My test exception'));



//
//
//        // Return passed exception, so it can be thrown.
//        $logManager->ex(new LogicException('My exception'));


//        $logManager->getAdminLog()->exception(new \Exception('Admin ex'));
//        $logManager->getTraderLog()->exception(new \Exception('Trader ex'));
//        $logManager->getTxLog()->exception(new \Exception('Tx ex'));
//        $logManager->getSystemLog()->exception(new \Exception('Sys ex'));

//        dump('Def log testd');

        return $this->render('@ISLog/Default/index.html.twig');
    }
}
