<?php

namespace InfiniteSoftware\Bundle\ISLogBundle\Controller;

use InfiniteSoftware\Bundle\ISLogBundle\Form\LogEntrySearchType;
use InfiniteSoftware\Bundle\ISLogBundle\Form\SystemLogEntrySearchType;
use InfiniteSoftware\Bundle\ISLogBundle\Services\LogManager;
use InfiniteSoftware\Bundle\ISLogBundle\Stuff\LogEntrySearch;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function systemLogAction(Request $request)
    {
        $logManager = $this->get('is_log.manager.log_manager');

        $entrySearch = new LogEntrySearch($logManager::LOG_TYPE_SYS);

        $form = $this->createForm(SystemLogEntrySearchType::class, $entrySearch);

        $form->handleRequest($request);

        // If AJAX request and if form is submitted and is valid.
        if ( $request->isXmlHttpRequest() && $form->isSubmitted() && $form->isValid())
        {
            $log = $logManager->getSystemLog($form->getData());

            return $this->render('@ISLog/SystemLog/entries_list.html.twig', [
                'logManager' => $logManager,
                'entries' => $log['entries'],
                'total' => $log['total'],
                'lastPageNum' => $log['lastPageNum']
            ]);
        }
        else
        {
            $log = $logManager->getSystemLog($form->getData());

            return $this->render('@ISLog/SystemLog/system_log.html.twig', [
                'logManager' => $logManager,
                'form' => $form->createView(),
                'entries' => $log['entries'],
                'total' => $log['total'],
                'lastPageNum' => $log['lastPageNum']
            ]);
        }
    }

    /**
     * @param Request $request
     * @param int $logType
     * @return Response
     */
    private function getAppLog(Request $request, $logType = null)
    {
        $logManager = $this->get('is_log.manager.log_manager');

        $entrySearch = new LogEntrySearch();
        $entrySearch->setLogType($logType);

        $form = $this->createForm(LogEntrySearchType::class, $entrySearch);

        $form->handleRequest($request);

        // If AJAX request and if form is submitted and is valid.
        if ( $request->isXmlHttpRequest() && $form->isSubmitted() && $form->isValid())
        {
            $log = $logManager->getAppLog($entrySearch);

            return $this->render('@ISLog/AppLog/entries_list.html.twig', [
                'logManager' => $logManager,
                'entrySearch' => $entrySearch,
                'entries' => $log['entries'],
                'total' => $log['total'],
                'limit' => $log['limit'],
                'lastPageNum' => $log['lastPageNum']
            ]);
        }
        else
        {
            $log = $logManager->getAppLog($entrySearch);

            return $this->render('@ISLog/AppLog/app_log.html.twig', [
                'logManager' => $logManager,
                'entrySearch' => $entrySearch,
                'form' => $form->createView(),
                'entries' => $log['entries'],
                'total' => $log['total'],
                'limit' => $log['limit'],
                'lastPageNum' => $log['lastPageNum']
            ]);
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function appLogAction(Request $request)
    {
        return $this->getAppLog($request);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function AdminLogAction(Request $request)
    {
        return $this->getAppLog($request, LogManager::LOG_TYPE_ADMIN);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function TraderLogAction(Request $request)
    {
        return $this->getAppLog($request, LogManager::LOG_TYPE_TRADER);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function TransactionLogAction(Request $request)
    {
        return $this->getAppLog($request, LogManager::LOG_TYPE_TRANSACTION);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteEntriesAction(Request $request)
    {
        $logManager = $this->get('is_log.manager.log_manager');

        $logManager->deleteEntryByIds(
            (int) $request->request->get('logType'),
            (int) $request->request->get('listLimit'),
            $request->request->get('item_ids')
        );

        $this->addFlash('notice', [
            'status' => 'success',
            'msg' => 'Entry(ies) is successfully deleted!'
        ]);

        return new JsonResponse([
            'success' => true,
            'redirect_url' => ''
        ]);
    }

    /**
     * @param Request $request
     * @param string $logType
     * @return Response
     */
    public function emptyLogAction(Request $request, $logType)
    {
        $logManager = $this->get('is_log.manager.log_manager');
        $logManager->emptyLog((int) $logType);

        switch ($logType)
        {
            case $logManager::LOG_TYPE_SYS:
                return $this->redirectToRoute('is_log_system');
                break;
            case $logManager::LOG_TYPE_ADMIN:
                return $this->redirectToRoute('is_log_admin');
                break;
            case $logManager::LOG_TYPE_TRADER:
                return $this->redirectToRoute('is_log_trader');
                break;
            case $logManager::LOG_TYPE_TRANSACTION:
                return $this->redirectToRoute('is_log_transaction');
                break;
            case $logManager::LOG_TYPE_APP:
                return $this->redirectToRoute('is_log_app');
                break;
            default: return $this->redirectToRoute('is_log_system');
        }
    }
}
