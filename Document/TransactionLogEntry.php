<?php

/**
 * Created by PhpStorm.
 * Email: molarro@gmail.com
 * Date: 29.03.2017
 */

namespace InfiniteSoftware\Bundle\ISLogBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use InfiniteSoftware\Bundle\ISLogBundle\Services\LogManager;

/**
 * Collection will be automatically created if it is not existing, during entry inserting.
 * @MongoDB\Document(collection="transaction_log_entries", repositoryClass="InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository\TransactionLogEntryRepository")
 */
class TransactionLogEntry
{
    use UtilityTrait;

    /**
     * @var string
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $txCode;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $createdAt;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $txType;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $status;

    /**
     * TODO: Maybe define tags in constants.
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $tag;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $message;

    /**
     * TransactionLogEntry constructor.
     * @param $txType
     * @param $txCode
     * @param $status
     * @param $message
     * @throws \Exception
     */
    public function __construct($txType, $txCode, $status, $message)
    {
        if ($status != LogManager::STATUS_INFO && $status != LogManager::STATUS_WARNING && $status != LogManager::STATUS_DANGER)
        {
            throw new \Exception('Invalid log entry status!');
        }

        $this->txType = $txType;
        $this->txCode = $txCode;
        $this->status = $status;
        $this->message = $message;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTxCode()
    {
        return $this->txCode;
    }

    /**
     * @param string $txCode
     * @return TransactionLogEntry
     */
    public function setTxCode($txCode)
    {
        $this->txCode = $txCode;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getTxType()
    {
        return $this->txType;
    }

    /**
     * @param string $txType
     */
    public function setTxType($txType)
    {
        $this->txType = $txType;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     * @return TransactionLogEntry
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }
}
