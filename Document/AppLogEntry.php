<?php

/**
 * Created by PhpStorm.
 * Email: molarro@gmail.com
 * Date: 29.03.2017
 */

namespace InfiniteSoftware\Bundle\ISLogBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use InfiniteSoftware\Bundle\ISLogBundle\Services\LogManager;

/**
 * Collection will be automatically created if it is not existing, during entry inserting.
 * @MongoDB\Document(collection="app_log_entries", repositoryClass="InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository\AppLogEntryRepository")
 */
class AppLogEntry
{
    use UtilityTrait;


    /**
     * @var string
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @var string
     * @MongoDB\Field(type="integer")
     */
    protected $logType;

    /**
     * @var integer
     * @MongoDB\Field(type="integer")
     */
    protected $traderId;

    /**
     * @var integer
     * @MongoDB\Field(type="integer")
     */
    protected $adminId;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $createdAt;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $status;

    /**
     * TODO: Define actions in constants.
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $action;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $message;

    /**
     * TODO: Maybe define tags in constants.
     *
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $tag;

    // ---- Transaction ------ //
    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $txCode;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $txType;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $txStatus;

    /**
     * @var bool
     * @MongoDB\Field(type="boolean")
     */
    protected $txSub;


    public function __construct($logType, $status, $message)
    {
        if ($status != LogManager::STATUS_INFO && $status != LogManager::STATUS_WARNING && $status != LogManager::STATUS_DANGER)
        {
            throw new \Exception("Invalid log entry status!");
        }

        $this->logType = $logType;
        $this->status = $status;
        $this->message = $message;

        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLogType()
    {
        return $this->logType;
    }

    /**
     * @param string $logType
     */
    public function setLogType($logType)
    {
        $this->logType = $logType;
    }

    /**
     * @return int
     */
    public function getTraderId()
    {
        return $this->traderId;
    }

    /**
     * @param int $traderId
     */
    public function setTraderId($traderId)
    {
        $this->traderId = $traderId;
    }

    /**
     * @return int
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $adminId
     */
    public function setAdminId($adminId)
    {
        $this->adminId = $adminId;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return string
     */
    public function getTxCode()
    {
        return $this->txCode;
    }

    /**
     * @param string $txCode
     */
    public function setTxCode($txCode)
    {
        $this->txCode = $txCode;
    }

    /**
     * @return string
     */
    public function getTxType()
    {
        return $this->txType;
    }

    /**
     * @param string $txType
     */
    public function setTxType($txType)
    {
        $this->txType = $txType;
    }

    /**
     * @return string
     */
    public function getTxStatus()
    {
        return $this->txStatus;
    }

    /**
     * @param string $txStatus
     */
    public function setTxStatus($txStatus)
    {
        $this->txStatus = $txStatus;
    }

    /**
     * @return bool
     */
    public function getTxSub()
    {
        return $this->txSub;
    }

    /**
     * @param bool $txSub
     */
    public function setTxSub($txSub)
    {
        $this->txSub = $txSub;
    }
}
