<?php

/**
 * Created by PhpStorm.
 * Email: molarro@gmail.com
 * Date: 29.03.2017
 */

namespace InfiniteSoftware\Bundle\ISLogBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use InfiniteSoftware\Bundle\ISLogBundle\Services\LogManager;

/**
 * Collection will be automatically created if it is not existing, during entry inserting.
 * @MongoDB\Document(collection="system_log_entries", repositoryClass="InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository\SystemLogEntryRepository")
 */
class SystemLogEntry
{
    use UtilityTrait;

    /**
     * @var string
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $createdAt;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $status;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $component;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $exception;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $code;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $message;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $file;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $line;

    /**
     * TODO: Maybe define tags in constants.
     *
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $tag;


    /**
     * SystemLogEntry constructor.
     * @param $status
     * @param $message
     * @param $component
     * @throws \Exception
     */
    public function __construct($status, $message, $component)
    {
        if ($status != LogManager::STATUS_INFO && $status != LogManager::STATUS_WARNING && $status != LogManager::STATUS_DANGER)
        {
            throw new \Exception('Invalid log entry status!');
        }

        $this->status = $status;
        $this->message = $message;
        $this->component = $component;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getComponent()
    {
        return $this->component;
    }

    /**
     * @return string
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param string $exception
     */
    public function setException($exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * @param string $line
     */
    public function setLine($line)
    {
        $this->line = $line;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     * @return SystemLogEntry
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
        return $this;
    }
}
