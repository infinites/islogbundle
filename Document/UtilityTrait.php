<?php
/**
 * Created by PhpStorm.
 * Email: molarro@gmail.com
 * Date: 27.09.2016
 */

namespace InfiniteSoftware\Bundle\ISLogBundle\Document;

trait UtilityTrait
{
    /**
     * @return string
     */
    public function getClassShortName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}