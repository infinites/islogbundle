<?php

/**
 * Created by PhpStorm.
 * Email: molarro@gmail.com
 * Date: 29.03.2017
 */

namespace InfiniteSoftware\Bundle\ISLogBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use InfiniteSoftware\Bundle\ISLogBundle\Services\LogManager;

/**
 * Collection will be automatically created if it is not existing, during entry inserting.
 * @MongoDB\Document(collection="admin_log_entries", repositoryClass="InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository\AdminLogEntryRepository")
 */
class AdminLogEntry
{
    use UtilityTrait;

    /**
     * @var string
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @var integer
     * @MongoDB\Field(type="integer")
     */
    protected $adminId;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $createdAt;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $status;

    /**
     * TODO: Define actions in constants.
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $action;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $reason;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $message;


    /**
     * AdminLogEntry constructor.
     * @param $adminId
     * @param $status
     * @param $message
     * @throws \Exception
     */
    public function __construct($adminId, $status, $message)
    {
        if ($status != LogManager::STATUS_INFO && $status != LogManager::STATUS_WARNING && $status != LogManager::STATUS_DANGER)
        {
            throw new \Exception('Invalid log entry status!');
        }

        $this->adminId = $adminId;
        $this->status = $status;
        $this->message = $message;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $adminId
     * @return AdminLogEntry
     */
    public function setAdminId($adminId)
    {
        $this->adminId = $adminId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return AdminLogEntry
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return AdminLogEntry
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}
