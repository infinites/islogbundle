<?php

/**
 * Created by PhpStorm.
 * Email: molarro@gmail.com
 * Date: 29.03.2017
 */

namespace InfiniteSoftware\Bundle\ISLogBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use InfiniteSoftware\Bundle\ISLogBundle\Services\LogManager;

/**
 * Collection will be automatically created if it is not existing, during entry inserting.
 * @MongoDB\Document(collection="trader_log_entries", repositoryClass="InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository\TraderLogEntryRepository")
 */
class TraderLogEntry
{
    use UtilityTrait;

    /**
     * @var string
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @var integer
     * @MongoDB\Field(type="integer")
     */
    protected $traderId;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $createdAt;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $status;

    /**
     * TODO: Maybe define actions in constants.
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $action;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $message;


    /**
     * TraderLogEntry constructor.
     * @param $traderId
     * @param $status
     * @param $message
     * @throws \Exception
     */
    public function __construct($traderId, $status, $message)
    {
        if ($status != LogManager::STATUS_INFO && $status != LogManager::STATUS_WARNING && $status != LogManager::STATUS_DANGER)
        {
            throw new \Exception('Invalid log entry status!');
        }

        $this->traderId = $traderId;
        $this->status = $status;
        $this->message = $message;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTraderId()
    {
        return $this->traderId;
    }

    /**
     * @param int $traderId
     * @return TraderLogEntry
     */
    public function setTraderId($traderId)
    {
        $this->traderId = $traderId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return TraderLogEntry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return TraderLogEntry
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }
}
