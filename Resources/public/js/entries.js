var selectors = {
    // Search
    itemList: '.js_item_list',
    totalEntries: '.js_total_entries',
    searchForm: '.js_search_form',
    offset: '.js_offset',
    pagination: '.js_pagination',

    // Action
    entry: '.js_entry',
    itemCheckbox: '.js_item_checkbox',
    entryDeleteBtn: '.js_entry_delete_btn',
    selectAllBtn: '.js_select_all_btn',
    actionSelect: '.js_action_select',
    actionMakeBtn: '.js_action_make_btn'
};

var Config = {};

// Constructor.
(function LogEntriesSearch(ServerConfig, selectors) {
    Config.server = ServerConfig;

    Controller(Model(), View(selectors));

})(ServerConfig, selectors);


function Model() {
    var _callbacks = {};

    function _request (reqParams, callback) {

        var request = $.ajax({
            url: reqParams.action,
            method: reqParams.method,
            data: reqParams.data,
            dataType: reqParams.type
        });

        request.done(function (msg) {
            //callback(msg, callback.context);

            _callbacks[callback[0]](msg, callback[1])
        });

        request.fail(function (msg) {
            alert('AJAX Fail! Check console');
            console.log(msg);
        });
    }

    function commonReq(reqParams, callback) {
        _request(reqParams, callback);
    }

    function setCallbacks(callbacks) {
        _callbacks = callbacks
    }

    return {
        setCallbacks: setCallbacks,
        commonReq: commonReq
    }

}

function View(selectors) {
    // Serialized form.
    var _lastForm = {};

    var _selectors = {};

    (function init() {
        _selectors = selectors;
    })();

    function getElem(selector) {
        return $(_selectors[selector]);
    }

    function getSelector(selectorName) {
        return _selectors[selectorName];
    }
    //------------------------------------//

    function _removeElem($elem) {
        $elem.remove();
    }

    function setPage(result, thisElem) {
        getElem('itemList').html(result);

        loadEffect(false);

        window.scrollTo(0, 0);
    }

    function deleteEntry(response, context) {
        context.$entry.remove();

        getElem('totalEntries').text(response['total_entries']);

        context.updatePagination(response['last_page_num']);

        loadEffect(false);
    }

    function checkItems(state) {
        if (state) {
            getElem('itemList').find("input[type='checkbox']").prop('checked', true);
        }
        else {
            getElem('itemList').find("input[type='checkbox']").prop('checked', false);
        }
    }

    /**
     * @returns {Array}
     */
    function getSelectedItemIds() {
        var ids = [];

        $.each(getElem('itemList').find("input[type='checkbox']"), function(index, elem) {

            if ($(elem).val() && $(elem).prop('checked')) {
                ids.push($(elem).val());
            }
        });

        return ids;
    }

    function setLastForm(lastForm) {
        _lastForm = lastForm;
    }

    function getLastForm() {
        return _lastForm;
    }

    function actionResult(response) {
        if (response['success']) {
           window.location.href = response['redirect_url'];
        }
    }

    /**
     * @param {boolean} state
     */
    function loadEffect(state) {
        if (state) getElem('itemList').css('opacity', 0.5);
        else getElem('itemList').css('opacity', 1);
    }

    return {
        getSelector: getSelector,
        getElem: getElem,
        setPage: setPage,
        deleteEntry: deleteEntry,
        loadEffect: loadEffect,
        setLastForm: setLastForm,
        getLastForm: getLastForm,
        checkItems: checkItems,
        getSelectedItemIds: getSelectedItemIds,
        actionResult: actionResult
    };
}

function Controller(M, V) {
    var defaultPaginOpts = {
        totalPages: Config.server.pagination.lastPageNum,
        initiateStartPageClick: false,
        visiblePages: 7, // max visible
        onPageClick: function (event, page) {
            search(((page - 1) * Config.server.pagination.listLimit), 'getPage')
        }
    };

    var callbacks = {
        getPage: function (response, context) {
            V.setPage(response);
        },

        actionResult: function (response, context) {
            V.actionResult(response);
        }
    };

    V.getElem('pagination').twbsPagination(defaultPaginOpts);

    function updatePagination(lastPageNum) {
        var currentPage = V.getElem('pagination').twbsPagination('getCurrentPage');
        V.getElem('pagination').twbsPagination('destroy');
        V.getElem('pagination').twbsPagination($.extend({}, defaultPaginOpts, {
            startPage: currentPage,
            totalPages: lastPageNum
        }));
    }

    function search(offset, callback) {
        V.loadEffect(true);

        V.getElem('offset').val(offset);

        V.setLastForm(V.getElem('searchForm').serialize());

        M.commonReq({
            method: Config.server.pagination.method,
            action: Config.server.pagination.action,
            data: V.getLastForm(),
            type: 'html'
        }, [callback]);
    }

    // V.getElem('itemList').on('click', V.getSelector('entryDeleteBtn'), function(event) {
    //     event.preventDefault();
    //     var thisElem = $(this);
    //
    //     var context = {
    //         $entry: thisElem.closest(V.getSelector('entry')),
    //         updatePagination: updatePagination
    //     };
    //
    //     V.loadEffect(true);
    //
    //     M.commonReq({
    //         method: thisElem.data('method'),
    //         action: thisElem.data('action'),
    //         type: 'json',
    //         data: {entry_id: thisElem.data('entry_id')}
    //     }, ['deleteEntry', context]);
    // });

    V.getElem('selectAllBtn').on('change', function() {
        V.checkItems($(this).prop('checked'));
    });

    V.getElem('itemCheckbox').on('click', function() {
        V.getElem('selectAllBtn').prop('checked', false);
    });

    V.getElem('actionMakeBtn').on('click', function() {
        var selectedAction = V.getElem('actionSelect').find(':selected');

        M.commonReq({
            method: selectedAction.data('method'),
            action: selectedAction.data('action'),
            data: {'logType': Config.server.logType, 'listLimit': Config.server.pagination.listLimit, 'action': V.getElem('actionSelect').val(), 'item_ids': V.getSelectedItemIds()},
            format: 'json'
        }, ['actionResult', {}]);
    });

    (function init() {
        M.setCallbacks(callbacks);

        V.setLastForm(V.getElem('searchForm').serialize());
    })();
}


