/**
 * Created by molarro on 18.8.1.
 */

$(function() {
    // Panels
    $('.clickable').on('click',function(){
        var effect = $(this).data('effect');
        $(this).closest('.panel')[effect]();
    });
});
