<?php

/**
 * Created by PhpStorm.
 * Email: molarro@gmail.com
 * Date: 29.03.2017
 */

namespace InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use InfiniteSoftware\Bundle\ISLogBundle\Stuff\LogEntrySearch;

class SystemLogEntryRepository extends DocumentRepository
{
    /**
     * About cursors - https://docs.mongodb.com/manual/tutorial/iterate-a-cursor/
     *
     * @param LogEntrySearch $es
     * @return mixed - MongoDb cursor to iterate (foreach).
     */
    public function getLog(LogEntrySearch $es)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->sort('createdAt', 'DESC')
            ->limit($es->getLimit())
            ->skip($es->getOffset()) // Offset.
        ;

        return $qb->getQuery()->execute();
    }


    public function emptyLog()
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->remove()
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function deleteEntryByIds(array $ids)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->remove()->field('_id')->in($ids)
        ;

        return $qb->getQuery()->execute();
    }

    public function countLogEntries()
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->count()
        ;

        return $qb->getQuery()->execute();
    }
}
