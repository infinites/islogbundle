<?php

/**
 * Created by PhpStorm.
 * Email: molarro@gmail.com
 * Date: 29.03.2017
 */

namespace InfiniteSoftware\Bundle\ISLogBundle\DocumentRepository;

use Doctrine\MongoDB\EagerCursor;
use Doctrine\ODM\MongoDB\DocumentRepository;
use InfiniteSoftware\Bundle\ISLogBundle\Services\LogManager;
use InfiniteSoftware\Bundle\ISLogBundle\Stuff\LogEntrySearch;

class AppLogEntryRepository extends DocumentRepository
{
    /**
     * About cursors - https://docs.mongodb.com/manual/tutorial/iterate-a-cursor/
     *
     * @param LogEntrySearch $es
     * @return EagerCursor - MongoDb cursor to iterate (foreach).
     */
    public function getLog(LogEntrySearch $es)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->sort($es->getSortField(), $es->getSortMethod())
        ;

        if ($es->getLimit() !== null)
        {
            $qb->limit($es->getLimit());
        }

        if ($es->getOffset() !== null)
        {
            $qb->skip($es->getOffset());
        }

        if ($es->getLogType() !== null)
        {
            $qb->field('logType')->equals($es->getLogType());
        }

        if ($es->getSearchField() && $es->getSearchString())
        {
            if (is_numeric($es->getSearchString()))
            {
                $qb->field($es->getSearchField())->equals((int) $es->getSearchString());
            }
            else
            {
                $qb->field($es->getSearchField())->equals(new \MongoRegex('/' . $es->getSearchString() . '/'));
            }
        }


        if ($es->getSubTx() !== null)
        {
            $qb->field('txSub')->in([$es->getSubTx(), null]);
        }

        return $qb->getQuery()->execute();
    }

    public function emptyLog()
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->remove()
        ;

        return $qb->getQuery()->execute();
    }

    public function countLogEntries()
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->count()
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function deleteEntryByIds(array $ids)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->remove()->field('_id')->in($ids)
        ;

        return $qb->getQuery()->execute();
    }
}
