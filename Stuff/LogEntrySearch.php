<?php

/**
 * Created by PhpStorm.
 * Author: Artem Molchanov
 * Email: molarro@gmail.com
 * Date: 30.03.2017
 */

namespace InfiniteSoftware\Bundle\ISLogBundle\Stuff;

/**
 * Class LogEntrySearch
 * @package InfiniteSoftware\Bundle\ISLogBundle\Stuff
 */
class LogEntrySearch
{
    /**
     * @var string
     */
    private $logType;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $offset;

    /**
     * @var int
     */
    private $sortField;

    private $sortMethod;

    private $searchField;
    private $searchString;

    /**
     * @var bool|null
     */
    private $subTx;

    /**
     * LogEntrySearch constructor.
     * @param $logType
     */
    function __construct($logType = null)
    {
        $this->logType = $logType;

        $this->offset = 0;
        $this->limit = 15;

        $this->sortField = 'createdAt';
        $this->sortMethod = 'DESC';
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return string
     */
    public function getLogType()
    {
        return $this->logType;
    }

    /**
     * @param string $logType
     */
    public function setLogType($logType)
    {
        $this->logType = $logType;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getSortField()
    {
        return $this->sortField;
    }

    /**
     * @param int $sortField
     */
    public function setSortField($sortField)
    {
        $this->sortField = $sortField;
    }

    /**
     * @return string
     */
    public function getSortMethod()
    {
        return $this->sortMethod;
    }

    /**
     * @param string $sortMethod
     */
    public function setSortMethod($sortMethod)
    {
        $this->sortMethod = $sortMethod;
    }

    /**
     * @return mixed
     */
    public function getSearchField()
    {
        return $this->searchField;
    }

    /**
     * @param mixed $searchField
     */
    public function setSearchField($searchField)
    {
        $this->searchField = $searchField;
    }

    /**
     * @return mixed
     */
    public function getSearchString()
    {
        return $this->searchString;
    }

    /**
     * @param mixed $searchString
     */
    public function setSearchString($searchString)
    {
        $this->searchString = $searchString;
    }

    /**
     * @return bool|null
     */
    public function getSubTx()
    {
        return $this->subTx;
    }

    /**
     * @param bool|null $subTx
     */
    public function setSubTx($subTx)
    {
        $this->subTx = $subTx;
    }
}